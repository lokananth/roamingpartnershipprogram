<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('session');
		$this->load->view('header_view');				
		$this->load->view('login_view');
		$this->load->view('footer_view');
	}
	
	public function checkUserLogin(){
		//echo 'hai';exit;
		//echo '<pre>';print_r($_REQUEST);exit;
		$this->load->library('session');
		
		$varUsername = trim($_REQUEST['username']);
		$varPassword = trim($_REQUEST['password']);
		
		$params = array('username'=>$varUsername,'Password'=>$varPassword);
		$arrLoginInfo = ApiPostHeader($this->config->item('UserLogin'), $params);
		//echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;				
		if(trim($arrLoginInfo['errcode'])=='0' && trim($arrLoginInfo['errcode'])!=''){			
			session_start();
			$_SESSION['userId'] = $arrLoginInfo['Id'];
			$_SESSION['userName'] = $varUsername;
			$_SESSION['userEmail'] = $arrLoginInfo['useremail'];			
			$_SESSION['userType'] = $arrLoginInfo['usertype'];
			redirect('customer');			
		}else{
			$this->session->set_flashdata('errmsg','Please enter a valid username & password');
			redirect('login');
		}		
	}
	
	public function UsersignOut(){
		session_start();
		session_destroy();
		session_unset();
		unset($_SESSION['userName']);
		redirect('login');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */