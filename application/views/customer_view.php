<?php //include('header.php');?>
<?php //include('inner-menu.php');?>
<?php //include('left-menu.php');?>
		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-reset-msg="Would you like to RESET all your saved widgets and clear LocalStorage?"><i class="fa fa-refresh"></i></span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb"><li>Home</li><li>View Number List</li></ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right" style="margin-right:25px">
					<a href="#" id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa fa-grid"></i> Change Grid</a>
					<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa fa-plus"></i> Add</span>
					<button id="search" class="btn btn-ribbon" data-title="search"><i class="fa fa-search"></i> <span class="hidden-mobile">Search</span></button>
				</span> -->

			</div>
		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> View Number List </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<!-- <span class="widget-icon"> <i class="fa fa-table"></i> </span> -->
									<h2>Search </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<form name="searchCustomerListform" class="form-horizontal" id="searchCustomerListform" method="post" action="<?php echo base_url();?>customer/searchByCustomerList">
													<div class="col-sm-12 col-md-12">
														<div class="form-group">
															<label class="col-sm-4  col-md-3 text-left">Search By:<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-3">
																<!--select class="form-control">
																	<option>In Use</option>
																	<option>Not In Use</option>
																</select-->
																
																<select class="form-control" name="searchKey" id="searchKey">
																	<option value="">Select</option>
																	<option value="1" <?php if(isset($varSearch) && $varSearch!='' && $varSearch=='1'){ ?> selected <?php } ?>>Mobile Number</option>
																	<option value="2" <?php if(isset($varSearch) && $varSearch!='' && $varSearch=='2'){ ?> selected <?php } ?>>Connection Type</option>
																	<option value="3" <?php if(isset($varSearch) && $varSearch!='' && $varSearch=='3'){ ?> selected <?php } ?>>Roaming Country Name</option>
																	<option value="4" <?php if(isset($varSearch) && $varSearch!='' && $varSearch=='4'){ ?> selected <?php } ?>>IMSI</option>																	
																</select>	
															</div>
															<div class="col-sm-3">
																<!--select class="form-control">
																	<option>UK</option>
																	<option>AT</option>
																	<option>SE</option>
																	<option>PT</option>
																</select-->
																<input class="form-control" type="text" name="searchValue" id="searchValue" value="" maxlength="25" autocomplete="off" placeholder="Enter value here" />
															</div>
															<div class="col-sm-3">
																<button type="submit" class="btn btn-primary">Search</button>
															</div>
														</div>														
													</div>													
												</form>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<!-- <span class="widget-icon"> <i class="fa fa-table"></i> </span> -->
									<h2>Customer List </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">			
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Mobile No</th>
													<th data-hide="phone">First Update</th>
													<th data-hide="phone">Last Update</th>
													<th data-hide="phone,tablet">Roaming Country</th>
													<th data-hide="phone,tablet">Roaming Since</th>
													<th data-hide="phone,tablet">Connection Type</th>
													<th data-hide="phone,tablet">Actual Balance</th>
													<th data-hide="phone,tablet">Converted Balance</th>
													<th data-hide="phone,tablet">Available Balance</th>
													<th data-hide="phone,tablet">Active IMSI</th>
													<th data-hide="phone,tablet">Left On</th>
													<th data-hide="phone,tablet">Threshold Limit</th>
													<th data-hide="phone,tablet">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>29.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><select class="form-control">
															<option>View Call History</option>
															<option>Change Threshold Limit</option>
														</select>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><a href="#" data-toggle="modal" data-target="#myModal2">Retire</a>											</td>
												</tr>
												
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><a href="#" data-toggle="modal" data-target="#myModal">Retire</a>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>40</td>
													<td><a href="#" data-toggle="modal" data-target="#myModal3">Retire</a>
													</td>
												</tr>
												
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><a href="#" id="modal3">Retire</a>
													</td>
												</tr>
												
												<tr>
													<td>0586932489556</td>
													<td>05-01-2016</td>
													<td>NA</td>
													<td>Sweden</td>
													<td>05-01-2016</td>
													<td>PostPaid</td>
													<td>Rs. 163.00</td>
													<td>Kroner 3.65</td>
													<td>Kroner 2.95</td>
													<td>58852329956329632</td>
													<td>NA</td>
													<td>20.00</td>
													<td><a href="#" id="modal3">Retire</a>
													</td>
												</tr>
												
																							
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade large" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Call History</h4>
							</div>
							<div class="modal-body">
								<div class="row m-t-20">
									<div class="col-md-12">
										<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">History Type</th>
													<th data-hide="phone,tablet">Call & Time</th>
													<th data-hide="phone,tablet">Destination Number</th>
													<th data-hide="phone,tablet">Package Name</th>
													<th data-hide="phone,tablet">Type</th>
													<th data-hide="phone,tablet">Destination Code</th>
													<th data-hide="phone,tablet">Duration(Min:Sec)</th>
													<th data-hide="phone,tablet">Data Usage(in MB)</th>
													<th data-hide="phone,tablet">Tariff Class</th>
													<th data-hide="phone,tablet">Roaming Zone</th>
													<th data-hide="phone,tablet">Net Charges</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>												
												<tr>
													<td>call</td>
													<td>24-01-2015 11:58:44 AM</td>
													<td>441410408004</td>
													<td></td>
													<td></td>
													<td>UKMCOM_ALL_TelephoneFixed_441</td>
													<td>0:58 Min</td>
													<td>0.00 MB</td>
													<td>VMOM</td>
													<td>0</td>													
													<td>0.05</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-hidden="true" >
									Close
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
					<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Change Threshold Limit</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form class="form-horizontal" >
										<div class="form-group">
											<label class="col-sm-4 control-label">Current credit limit</label>
											<div class="col-sm-6">
												<p class="form-control-static">RS 200</p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">New credit limit</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" />
											</div>
										</div>
									</form>
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-primary" >
											Save
										</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	<!-- modal-->
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Threshold Limit Updated</h4>
							</div>
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">New Threshold Limit has been updated successfully</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									Close
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	<!-- modal end-->	
	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<?php //include('footer.php');?>
<script>
	$(document).ready(function (){
		
		$("#searchCustomerListform").validate({			
			rules: {
				searchKey: "required",
				searchValue: "required",
			},
			message: {
				searchKey: "Please select a search option",
				searchValue: "Please enter a search value",
			}			
		});
		
	});
</script>